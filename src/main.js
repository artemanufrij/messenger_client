import Vue from "vue";
import VueSocketIO from "vue-socket.io";
import App from "./App.vue";
import store from "./store";

Vue.use(
  new VueSocketIO({
    debug: true,
    connection: "https://home.anufrij.de",
    /* connection: "http://ubuntuserver:3010", */
/*     connection: "http://localhost:3010", */
    vuex: {
      store,
      actionPrefix: "SOCKET_",
      mutationPrefix: "SOCKET_"
    }
  })
);

new Vue({
  el: "#app",
  store,
  render: h => h(App)
});
