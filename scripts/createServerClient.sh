#!/bin/sh
rm -r ../messenger_server/client/*
cp -rf dist/ ../messenger_server/client
cp -rf static/ ../messenger_server/client
cp *.html ../messenger_server/client
cp manifest.json ../messenger_server/client